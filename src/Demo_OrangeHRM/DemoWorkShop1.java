package Demo_OrangeHRM;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWorkShop1 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
        driver.get("https://demowebshop.tricentis.com/login");
        driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElement(By.id("Email")).sendKeys("mbthirusha@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("Thirusha01@");
        driver.findElement(By.xpath("//input[@value='Log in']")).click();
        WebElement Computer= driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
        Actions act = new Actions(driver);
        act.moveToElement(Computer).build().perform();
        Thread.sleep(3000);
        act.moveToElement(driver.findElement(By.partialLinkText("Desktops"))).click().perform();
        WebElement element=driver.findElement(By.id("products-orderby"));
        Select sc = new Select(element);
        sc.selectByVisibleText("Price: Low to High");
        driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("add-to-cart-button-72")).click();
        Thread.sleep(3000);
        driver.findElement(By.linkText("Shopping cart")).click();
        driver.findElement(By.id("termsofservice")).click();
        driver.findElement(By.id("checkout")).click();
        WebElement selectcountry= driver.findElement(By.id("BillingNewAddress_CountryId"));
        Select sc1= new Select(selectcountry);

        sc1.selectByVisibleText("India");

        driver.findElement(By.id("BillingNewAddress_City")).sendKeys("Hosur");

        driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("Zuzuvadi");

        driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("635126");

       driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("6383553215");

        Thread.sleep(2000);
        driver.findElement(By.xpath("(//input[@title='Continue'])[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("PickUpInStore")).click();
        driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']")).click();
        Thread.sleep(2000);
        JavascriptExecutor js= (JavascriptExecutor)driver;
        js.executeScript("document.getElementById('confirm-order-buttons-container').scrollIntoView()");
        driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']")).click();
        Thread.sleep(2000);
	    
	     WebElement text= driver.findElement(By.xpath("//div[@class='section order-completed']"));
	    
		 System.out.println(text.getText());
		 driver.findElement(By.linkText("Log out")).click();
		 driver.close();
	}

}

